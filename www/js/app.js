// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('ionic.utils', [])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);


angular.module('api.service', [])

.service('apiService', function($http, $q, $localstorage, $ionicLoading, $cordovaNetwork) {

    var privateKey='C6uEVxbWi0NYiehwe8iQtZrT3P1EJXhKpUn1TCrU';
    var baseURL = 'http://api.franchiseinnovators.com/api/v1/';
    
    this.makeRandomKey = function()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 10; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    this.getFranquicias = function() {
        var defer = $q.defer();

        var randomKey = this.makeRandomKey();
        var accessKey = CryptoJS.SHA1(privateKey+randomKey);
        $http.defaults.useXDomain = true
          $http.get(baseURL+'franquicia/', {
              headers: {'X-AccessKey': accessKey,
                        'X-RandomKey': randomKey
                        }
          })        
          .success(function(data, status, headers, config) {
            if(data.result_count > 0){
              defer.resolve(data);  
            }else{
              defer.reject('No franchises in the database');
            }
          })
          .error(function(data, status, headers, config) {
              defer.reject('Error synchronizing information.');
          });  

        return defer.promise;
    },
    this.getMatches = function() {
        var defer = $q.defer();

        var randomKey = this.makeRandomKey();
        var accessKey = CryptoJS.SHA1(privateKey+randomKey);
        $http.defaults.useXDomain = true
          $http.get(baseURL+'match/user/'+$localstorage.get('id'), {
              headers: {'X-AccessKey': accessKey,
                        'X-RandomKey': randomKey
                        }
          })        
          .success(function(data, status, headers, config) {
            if(data.result_count > 0){
              defer.resolve(data);  
            }else{
              defer.reject('No matches for this user');
            }
          })
          .error(function(data, status, headers, config) {
              defer.reject('Error synchronizing information.');
          });  

        return defer.promise;
    },
    this.getNews = function() {
        
        var defer = $q.defer();

        var randomKey = this.makeRandomKey();
        var accessKey = CryptoJS.SHA1(privateKey+randomKey);
        $http.defaults.useXDomain = true
        //if($cordovaNetwork.isOnline()){
          $http.get(baseURL+'news/', {
              headers: {'X-AccessKey': accessKey,
                        'X-RandomKey': randomKey
                        }
          })        
          .success(function(data, status, headers, config) {
            if(data.result_count > 0){
              defer.resolve(data);  
            }else{
              defer.reject('No News in the database');
            }
          })
          .error(function(data, status, headers, config) {
              defer.reject('Error synchronizing information.');
          });  
        //}else{
        //  defer.reject("You don't have internet connection." );
        //}
        return defer.promise;
        
    },
    this.getQuestions = function() {
        var defer = $q.defer();
        $http.get('js/question.json')
            .success(function(data) {
              defer.resolve(data);
            })
            .error(function() {
                defer.reject('could not find question.json');
            });

        return defer.promise;
    },
    this.createUser = function(user){
      var defer = $q.defer();
      $ionicLoading.show({
          template: 'Creating your account...'
      });

      var randomKey = this.makeRandomKey();
      var accessKey = CryptoJS.SHA1(privateKey+randomKey);
      $http.defaults.useXDomain = true

      $http({
            url: baseURL+'user/',
            method: "POST",
            data: {name: user.firstname,
                   lastname: user.lastname,
                   email: user.correo,
                   password: user.password,
                   phone: ''},
            headers: {'X-AccessKey': accessKey,
                      'X-RandomKey': randomKey,
                      'Content-Type': 'application/json'
                      }
      })
        .success(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.resolve(data); 
        })
        .error(function (data, status, headers, config) {
          $ionicLoading.hide();
          if(status === 406)
            defer.reject(status);
          else    
            defer.reject(status);
        });

        return defer.promise;
      },
      this.loginUser = function(user){
      var defer = $q.defer();
      $ionicLoading.show({
          template: 'Signing in...'
      });

      var randomKey = this.makeRandomKey();
      var accessKey = CryptoJS.SHA1(privateKey+randomKey);
      $http.defaults.useXDomain = true

      $http({
            url: baseURL+'user/login/',
            method: "POST",
            data: {email: user.correo,
                   password: user.password
                   },
            headers: {'X-AccessKey': accessKey,
                      'X-RandomKey': randomKey,
                      'Content-Type': 'application/json'
                      }
      })
        .success(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.resolve(data); 
        })
        .error(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.reject(data);
        });

        return defer.promise;
      },
      this.updateAnswers = function(answersUser){
      var defer = $q.defer();
      $ionicLoading.show({
          template: 'Saving...'
      });

      var randomKey = this.makeRandomKey();
      var accessKey = CryptoJS.SHA1(privateKey+randomKey);
      $http.defaults.useXDomain = true;
      var object = { answers: answersUser}
      var jsonAnswers = JSON.stringify(object);
      $http({
            url: baseURL+'user/'+$localstorage.get('id')+'/',
            method: "PUT",
            data: jsonAnswers,
            headers: {'X-AccessKey': accessKey,
                      'X-RandomKey': randomKey,
                      'Content-Type': 'application/json'
                      }
      })
        .success(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.resolve(data); 
        })
        .error(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.reject(data);
        });

        return defer.promise;
      },
      this.uploadMatch = function(franq_id, result){
      var defer = $q.defer();
      $ionicLoading.show({
          template: 'Calculating...'
      });

      var randomKey = this.makeRandomKey();
      var accessKey = CryptoJS.SHA1(privateKey+randomKey);
      $http.defaults.useXDomain = true;
      
      $http({
            url: baseURL+'match/',
            method: "POST",
            data: {franq_id: franq_id.toString(),
                   user_id: $localstorage.get('id').toString(),
                   result: result.toString()
                  },
            headers: {'X-AccessKey': accessKey,
                      'X-RandomKey': randomKey
                      }
      })
        .success(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.resolve(data); 
        })
        .error(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.reject(data);
        });

        return defer.promise;
      },

      this.sendEmail = function(contact, franchise){
      var defer = $q.defer();
      $ionicLoading.show({
          template: 'Loading...'
      });

      var randomKey = this.makeRandomKey();
      var accessKey = CryptoJS.SHA1(privateKey+randomKey);
      $http.defaults.useXDomain = true;
      
      $http({
            url: baseURL+'user/contact/',
            method: "POST",
            data: {contact_name: contact.name,
                   email: contact.email,
                   phone: contact.phone,
                   franchise_name: franchise
                  },
            headers: {'X-AccessKey': accessKey,
                      'X-RandomKey': randomKey
                      }
      })
        .success(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.resolve(data); 
        })
        .error(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.reject(data);
        });

        return defer.promise;
      },
      this.changePassword = function(password){
      var defer = $q.defer();
      $ionicLoading.show({
          template: 'Loading...'
      });

      var randomKey = this.makeRandomKey();
      var accessKey = CryptoJS.SHA1(privateKey+randomKey);
      $http.defaults.useXDomain = true;
      
      $http({
            url: baseURL+'user/change-password/',
            method: "POST",
            data: {old_password: password.current,
                   email: $localstorage.get('username'),
                   new_password: password.new
                  },
            headers: {'X-AccessKey': accessKey,
                      'X-RandomKey': randomKey
                      }
      })
        .success(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.resolve(data); 
        })
        .error(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.reject(data);
        });

        return defer.promise;
      },
      this.forgotPassword = function(user){
      var defer = $q.defer();
      $ionicLoading.show({
          template: 'Loading...'
      });

      var randomKey = this.makeRandomKey();
      var accessKey = CryptoJS.SHA1(privateKey+randomKey);
      $http.defaults.useXDomain = true;
      
      $http({
            url: baseURL+'user/forget-password/',
            method: "POST",
            data: {email: user.correo
                  },
            headers: {'X-AccessKey': accessKey,
                      'X-RandomKey': randomKey
                      }
      })
        .success(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.resolve(data); 
        })
        .error(function (data, status, headers, config) {
          $ionicLoading.hide();
          defer.reject(data);
        });

        return defer.promise;
      }

});
//

angular.module('encuesta', ['ionic','ngCordova', 'ionic.utils', 'encuesta.controllers', 'encuesta.services', 'api.service'])

.run(function($ionicPlatform, $localstorage, $location, $window, SQLService, $cordovaSplashscreen, apiService, $state, $ionicLoading, $cordovaNetwork ) {
  $ionicPlatform.ready(function() {

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    $ionicPlatform.registerBackButtonAction(function (event) {
      /*
      if($ionicHistory.currentStateName() == "myiew"){
        ionic.Platform.exitApp();
        // or do nothing
      }
      else {
        $ionicHistory.goBack();
      }
      */
    }, 100);

    SQLService.setup();

    if($localstorage.get('username') === undefined || $localstorage.get('username') === ''){
      console.log("NO hay un user definido");
      window.setTimeout(function() {
        $cordovaSplashscreen.hide();
      }, 1500);
    }else{
        console.log("SI hay un user definido");
        
        if($cordovaNetwork.isOnline()){
            apiService.getFranquicias().then(function(data) {
                SQLService.deleteFranquicias();
                for (var i=0; i< data.result.length; i++){
                    SQLService.setFranquicia(data.result[i]);
                }
                $state.transitionTo('app.home');
                window.setTimeout(function() {
                  //$cordovaSplashscreen.hide();
                }, 5000)  
            }, function(message) {
                $ionicPopup.alert({
                    title: 'Error',
                    template: message
                });
            });
        }else{
          $state.transitionTo('app.home');
          window.setTimeout(function() {
            //$cordovaSplashscreen.hide();
          }, 5000)  
        }
    }
  });
})

.config(['$stateProvider','$urlRouterProvider','$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {
//.config(function($stateProvider, $urlRouterProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];


    $urlRouterProvider.otherwise('/index');
    $stateProvider

      .state('index', {
          url: '/index',
          templateUrl: 'templates/index.html',
          controller: 'indexCtrl'
      })

      .state('privacy', {
          url: '/privacy',
          templateUrl: 'templates/privacy.html',
          controller: 'privacyCtrl'
      })

      .state('publicity', {
          url: '/publicity',
          templateUrl: 'templates/publicity.html',
          controller: 'publiCtrl'
      })

      .state('interview', {
          cache: false,
          url: '/interview',
          templateUrl: 'templates/interview.html',
          controller: 'interviewCtrl'
      })

      .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'homeCtrl'
      })

      .state('app.home', {
        url: "/home",
        views: {
          'menuContent': {
            templateUrl: "templates/home.html"
          }
        }
      })

      .state('app.usfranchises', {
        url: "/usfranchises",
        views: {
          'menuContent': {
            templateUrl: "templates/usFranchises.html"
          }
        }
      })

      .state('app.international', {
        url: "/international",
        views: {
          'menuContent': {
            templateUrl: "templates/internationalFranchises.html"
          }
        }
      })

      .state('app.logout', {
        url: "/logout",
        views: {
          'menuContent': {
            templateUrl: "templates/logout.html",
            controller: 'logoutCtrl'
          }
        }
      })

      .state('app.account', {
        url: "/account",
        views: {
          'menuContent': {
            templateUrl: "templates/account.html"
          }
        }
      })

      .state('app.matchs', {
        url: "/matchs",
        views: {
          'menuContent': {
            templateUrl: "templates/matchs.html"
          }
        }
      })

      .state('app.match', {
        url: "/matchs/:idMatch",
        views: {
          'menuContent': {
            templateUrl: "templates/match.html"
          }
        }
      })

      .state('app.differences', {
        url: "/matchs/:idMatch/difference",
        views: {
          'menuContent': {
            templateUrl: "templates/differences.html"
          }
        }
      })

      .state('app.franchise', {
        url: "/home/:idFranchise",
        views: {
          'menuContent': {
            templateUrl: "templates/contact.html"
          }
        }
      })

      .state('app.contact', {
        url: "/home/contact",
        views: {
          'menuContent': {
            templateUrl: "templates/contact2.html"
          }
        }
      })

      .state('app.news', {
        url: "/news",
        views: {
          'menuContent': {
            templateUrl: "templates/news.html"
          }
        }
      })

  }    
]);
//});

