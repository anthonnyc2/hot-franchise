  angular.module('encuesta.services', [])

.factory("SQLService", function ($q) {

    var db;
    var task='';
    var deltask;
    
    function createDB() {
        try {
            db = window.openDatabase("encuestaDB3", "2.1", "EncuestaApp", 5*1024*1024);
            db.transaction(function(tx){
                //tx.executeSql('DROP TABLE franquicias');
                tx.executeSql("CREATE TABLE IF NOT EXISTS preferencias (id INTEGER NOT NULL, resp1 VARCHAR(10), resp2 VARCHAR(10), resp3 VARCHAR(10), resp4 VARCHAR(10), resp5 VARCHAR(10), resp6 VARCHAR(10), resp7 VARCHAR(10), resp8 VARCHAR(10), resp9 VARCHAR(10), resp10 VARCHAR(10) )",[]);
                tx.executeSql("CREATE TABLE IF NOT EXISTS franquicias (id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(255), description TEXT, url TEXT, type VARCHAR(50), answer TEXT, address TEXT, link_youtube TEXT, type_business TEXT, history TEXT, cash_investment TEXT, training TEXT, qualifications TEXT, url_logo TEXT, url_twitter TEXT, url_facebook TEXT, url_linkedin TEXT, url_google_plus TEXT, country TEXT, no_franchises TEXT, franchise_cost TEXT)",[]);
                tx.executeSql("CREATE TABLE IF NOT EXISTS matchs (id INTEGER NOT NULL PRIMARY KEY, name_franq VARCHAR(255), result INTEGER, date TEXT)",[]);
            });
        } catch (err) {
            alert("Error processing SQL: " + err);
        }
        console.log('database created');
    }

    function setPreferencias(answers){
        promisedQuery("DELETE FROM preferencias", actionDefaulHandler, defaultErrorHandler);
        return promisedQuery("INSERT INTO preferencias(id, resp1, resp2, resp3, resp4, resp5, resp6, resp7, resp8, resp9, resp10) VALUES ('1', '" + answers[1] + "','" + answers[2] + "','" + answers[3] + "','" + answers[4] + "','" + answers[5] + "','" + answers[6] + "','" + answers[7] + "','" + answers[8] + "','" + answers[9] + "','" + answers[10] + "')", actionDefaulHandler, defaultErrorHandler);
    }

    function setFranquicia(franquicia){
        var jsonString = escape(JSON.stringify(franquicia.answers));
        return promisedQuery("INSERT OR REPLACE INTO franquicias(id, name, description, url, type, answer, address, link_youtube, type_business, history, cash_investment, training, qualifications, url_logo, url_twitter, url_facebook, url_linkedin, url_google_plus, country, no_franchises, franchise_cost ) VALUES ("+franquicia.id+",'"+ escape(franquicia.name) + "','" + escape(franquicia.description) + "','" + franquicia.link + "','" + franquicia.type + "','"+jsonString+"','"+escape(franquicia.address)+"','"+franquicia.youtube_link+"','"+escape(franquicia.type_business)+"','"+escape(franquicia.history)+"','"+escape(franquicia.cash_investment)+"','"+escape(franquicia.training)+"','"+escape(franquicia.qualifications)+"','"+franquicia.url_logo+"','"+franquicia.url_twitter+"','"+franquicia.url_facebook+"','"+franquicia.url_linkedin+"','"+franquicia.url_google_plus+"','"+ franquicia.country+"','"+franquicia.no_franchises+"','"+franquicia.cost+ "')", actionDefaulHandler, defaultErrorHandler);
    }

    function setmatch(id, result, name_franq){
        var datetime = new Date();
        console.log(datetime.format("yyyy-mm-dd HH:MM:ss"));
        return promisedQuery("INSERT OR REPLACE INTO matchs(id, name_franq, result, date) VALUES ("+id+",'"+ escape(name_franq) + "'," + result + ",'" + datetime.format("yyyy-mm-dd HH:MM:ss") + "')", actionDefaulHandler, defaultErrorHandler);
    }  

    function getMatch(id){
        return promisedQuery('SELECT * FROM matchs WHERE id = '+id, resultMatchs, defaultErrorHandler);
    }
    
    function deleteData(){
        return promisedQuery("DELETE FROM preferencias", actionDefaulHandler, defaultErrorHandler);
    }
    
    function getPreferencias(){
        return promisedQuery('SELECT * FROM preferencias', resultsPreferencias, defaultErrorHandler);
    }

    function getFranquiciasUS(){
        return promisedQuery('SELECT * FROM franquicias where type = 2 ORDER BY name ASC', resultFranquicias, defaultErrorHandler);
    }

    function findFranquiciasUS(str){
        return promisedQuery('SELECT * FROM franquicias where type = 2 and name like "'+str+'%" ORDER BY name ASC', resultFranquicias, defaultErrorHandler);
    }

    function findFranquiciasInternational(str){
        return promisedQuery('SELECT * FROM franquicias where type = 1 and name like "'+str+'%" ORDER BY name ASC', resultFranquicias, defaultErrorHandler);
    }

    function findFranquiciasByNameAndCountry(str, country){
        return promisedQuery('SELECT * FROM franquicias where type = 1 AND country = "'+country+'" and name like "%'+str+'%" ORDER BY name ASC', resultFranquicias, defaultErrorHandler);
    }

    function findFranquiciasByCountry(country){
        console.log('SELECT * FROM franquicias where type = 1 and country = "'+country+'" ORDER BY name ASC');
        return promisedQuery('SELECT * FROM franquicias where type = 1 and country = "'+country+'" ORDER BY name ASC', resultFranquicias, defaultErrorHandler);
    }

    function getFranquiciasInternational(){
        return promisedQuery('SELECT * FROM franquicias where type = 1 ORDER BY name ASC', resultFranquicias, defaultErrorHandler);
    }

    function getCountries(){
        return promisedQuery('SELECT DISTINCT country FROM franquicias where type = 1 ORDER BY country ASC', resultCountries, defaultErrorHandler);
    }

    function getFranquicia(id){
        return promisedQuery('SELECT * FROM franquicias where id = '+id, resultFranquicias, defaultErrorHandler);
    }  


    function getMatchs(){
        return promisedQuery('SELECT * FROM matchs ORDER BY datetime(date) DESC ', resultMatchs, defaultErrorHandler);
    }

    function deleteMatchs(){
        return promisedQuery('DELETE FROM matchs', actionDefaulHandler, defaultErrorHandler); 
    }    

    function deleteFranquicias(){
        return promisedQuery('DELETE FROM franquicias', actionDefaulHandler, defaultErrorHandler); 
    }  

    function actionDefaulHandler(){
        console.log("case positive")
    }
    
    function resultFranquicias(deferred){
        return function(tx, results) {
            var len = results.rows.length;
            console.log("total franquicias "+len);
            var output_results = [];
            
            for (var i=0; i<len; i++){
                var t = {'id':results.rows.item(i).id,'name':unescape(results.rows.item(i).name), 'description':unescape(results.rows.item(i).description), 'url':results.rows.item(i).url, 'answer':results.rows.item(i).answer, 'url_twitter': results.rows.item(i).url_twitter, 'address': unescape(results.rows.item(i).address), 'url_linkedin': results.rows.item(i).url_linkedin, 'url_facebook': results.rows.item(i).url_facebook, 'url_google_plus': results.rows.item(i).url_google_plus, 'history': unescape(results.rows.item(i).history), 'link_youtube': results.rows.item(i).link_youtube, 'type': results.rows.item(i).type, 'type_business':unescape(results.rows.item(i).type_business), 'cash_investment':unescape(results.rows.item(i).cash_investment), 'training':unescape(results.rows.item(i).training), 'qualifications':unescape(results.rows.item(i).qualifications), 'url_logo':results.rows.item(i).url_logo, 'country':results.rows.item(i).country, 'no_franchises':results.rows.item(i).no_franchises, 'franchise_cost':results.rows.item(i).franchise_cost };
                output_results.push(t);             
            }
            
            deferred.resolve(output_results);  
        }  
    }

    function resultCountries(deferred){
        return function(tx, results) {
            var len = results.rows.length;
            console.log("total countries "+len);
            var output_results = [];
            
            for (var i=0; i<len; i++){
                var t = {'country':results.rows.item(i).country };
                output_results.push(t);             
            }
            
            deferred.resolve(output_results);  
        }  
    }
    
    function resultMatchs(deferred){
        return function(tx, results) {
            var len = results.rows.length;
            console.log("total resultMatchs "+len);
            var output_results = [];
                                                     
            for (var i=0; i<len; i++){
                var d = new Date(results.rows.item(i).date);
                var t = {'id':results.rows.item(i).id, 'name': unescape(results.rows.item(i).name_franq), 'result':results.rows.item(i).result, 'date':d};
                output_results.push(t);             
            }
            
            deferred.resolve(output_results);  
        }  
    }

    function resultsPreferencias(deferred) {
      return function(tx, results) {
        var len = results.rows.length;
        console.log("total preferencias "+len);
        var output_results = [];
        
        for (var i=0; i<len; i++){
            var t = {'resp1':results.rows.item(i).resp1, 'resp2':results.rows.item(i).resp2, 'resp3':results.rows.item(i).resp3, 'resp4':results.rows.item(i).resp4, 'resp5':results.rows.item(i).resp5, 'resp6':results.rows.item(i).resp6, 'resp7':results.rows.item(i).resp7, 'resp8':results.rows.item(i).resp8, 'resp9':results.rows.item(i).resp9, 'resp10':results.rows.item(i).resp10};
            output_results.push(t);             
        }
        
        deferred.resolve(output_results);  
      }  
    }
    
    function defaultErrorHandler(deferred) {
      return function(tx, results) {
        var len = 0;
        var output_results = ''; 
        deferred.resolve(output_results);
      } 
    }
    
    function promisedQuery(query, successCB, errorCB) {
      var deferred = $q.defer();
      db.transaction(function(tx){
        tx.executeSql(query, [], successCB(deferred), errorCB(deferred));      
      }, errorCB);
      return deferred.promise;  
    }
    
    return {
        setup: function() {
          return createDB();
        },
        setPreferencias: function(answers) {
            return setPreferencias(answers);
        },
        logout: function() {
            return deleteData();
        },
        setFranquicia: function (franquicia) {
            return setFranquicia(franquicia);
        },
        allPreferencias: function() {
          return getPreferencias();
        },
        allFranquiciasUS: function() {
          return getFranquiciasUS();
        },
        allFranquiciasInternational: function() {
          return getFranquiciasInternational();
        },
        findInternationalFranchisesByName: function(str){
            return findFranquiciasInternational(str);
        },
        findUsFranchisesByName: function(str){
            return findFranquiciasUS(str);
        },
        findInternationalFranchisesByNameAndCountry: function(str, country){
            return findFranquiciasByNameAndCountry(str, country);
        },
        findInternationalFranchisesByCountry: function(country){
            return findFranquiciasByCountry(country);
        },
        getFranquicia: function(id){
            return getFranquicia(id);
        },
        getAllCountries: function(){
            return getCountries();
        },
        deleteFranquicias: function() {
          return deleteFranquicias();
        },
        allMatchs: function() {
          return getMatchs();
        },
        setMatch: function(id, result, name_franq){
            return setmatch(id, result, name_franq);
        },
        getMatch: function(id){
            return getMatch(id);
        },
        deleteMatchs: function() {
          return deleteMatchs();
        },
        test: function(){
            console.log("test");
        }
    }
});