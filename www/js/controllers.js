angular.module('encuesta.controllers', ['encuesta.services'])

.controller('indexCtrl', function ($scope, $ionicModal, $state, $ionicLoading, $rootScope, $ionicPopup, $ionicLoading, $window, $localstorage, SQLService, apiService, $cordovaNetwork) {
    
    $scope.$on('$ionicView.beforeEnter', function(){
        $scope.user = {
            firstname : "", lastname: "", email: "", password: "", phone: ""
        };
    });
    
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal1 = modal;
    });

    $ionicModal.fromTemplateUrl('templates/signup.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal2 = modal;
    });

    $ionicModal.fromTemplateUrl('templates/forgot_password.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal3 = modal;
    });

    // CLose Modal
    $scope.closeModal = function(id) {
        if(id === 'login')
            $scope.modal1.hide();
        if(id === 'signup')
            $scope.modal2.hide();
        if(id === 'forgot')
            $scope.modal3.hide();

        $scope.user = {
            firstname : "", lastname: "", email: "", password: "", phone: ""
        };
    };

    // Open modal
    $scope.openModal = function(id) {
        $scope.user = {
            //firstname : "", lastname: "", correo: "anthonio@gmail.com", password: "password", phone: ""
            firstname : "", lastname: "", correo: "", password: "", phone: ""
        };
        if(id === 'login')
            $scope.modal1.show();
        if(id === 'signup')
            $scope.modal2.show();
        if(id === 'forgot')
            $scope.modal3.show();
    };

    $scope.createUser = function(){
        
        if(this.user.correo){
            if (this.user.correo.length === 0 || this.user.password.length === 0 || this.user.lastname.length === 0 || this.user.firstname.length === 0) {
                $ionicPopup.alert({
                    title: 'Hot Franchise',
                    template: 'Some fields are empty'
                });
              return false;
            }else{
                if(isValidEmail(this.user.correo)){
                    if($cordovaNetwork.isOnline()){
                        var response = apiService.createUser(this.user);
                        response.then(function(data) {
                            if(!data.error){
                                $ionicPopup.alert({
                                    title: 'Hot Franchise',
                                    template: "Your user profile was successfully created"
                                });
                                $localstorage.set('username', data.result.email);
                                $localstorage.set('id', data.result.id);
                                $scope.closeModal('signup');
                                $state.transitionTo('privacy');
                            }else{
                                $ionicPopup.alert({
                                    title: 'Error',
                                    template: data.message
                                });
                            }
                        }, function(data) {
                            if(data === 406){
                                $ionicPopup.alert({
                                    title: 'Hot Franchise',
                                    template: "This email is already used"
                                });
                            }else{
                                $ionicPopup.alert({
                                    title: 'Hot Franchise',
                                    template: 'Something went wrong... try again later'
                                });    
                            }
                        });    
                    }else{
                        $ionicPopup.alert({
                            title: 'Hot Franchise',
                            template: "You are unable to access the Internet"
                        });  
                    }
                }else{
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Invalid email address'
                    });
                }
            }    
        }else{
            $ionicPopup.alert({
                title: 'Error',
                template: 'Invalid email address'
            });
        }
        
    };

    $scope.modalForgotPassword = function(){
        this.closeModal('login');
        this.openModal('forgot');
    }

    $scope.forgotPassword = function(user){
        if(this.user.correo){
            if (this.user.correo.length === 0 ){
                $ionicPopup.alert({
                    title: 'Hot Franchise',
                    template: 'Some fields are empty'
                });
              return false;
            }else{
                if(isValidEmail(this.user.correo)){
                    if($cordovaNetwork.isOnline()){
                        var response = apiService.forgotPassword(this.user);
                        response.then(function(data) {
                            $ionicPopup.alert({
                                title: 'Hot Franchise',
                                template: 'You will receive an email with your temporary password.'
                            });
                            $scope.closeModal('forgot'); 
                        }, function(data) {
                            if(data.error === true){
                                $ionicPopup.alert({
                                    title: 'Hot Franchise',
                                    template: data.message
                                });
                            }else{
                                $ionicPopup.alert({
                                    title: 'Hot Franchise',
                                    template: 'Something went wrong... try again later'
                                }); 
                            }
                        });
                    }else{
                        $ionicPopup.alert({
                            title: 'Hot Franchise',
                            template: "You are unable to access the Internet"
                        });  
                    }

                }else{
                    $ionicPopup.alert({
                        title: 'Hot Franchise',
                        template: 'Invalid email address'
                    });

                }
            }    
        }else{
            $ionicPopup.alert({
                title: 'Hot Franchise',
                template: 'Invalid email address'
            });
        }
        
    }

    $scope.logIn = function(){
        if($scope.user.correo){
            if ($scope.user.correo.length === 0 || $scope.user.password.length === 0) {
                $ionicPopup.alert({
                    title: 'Hot Franchise',
                    template: 'Some fields are empty'
                });
              return false;
            }else{
                if(isValidEmail(this.user.correo)){
                    if($cordovaNetwork.isOnline()){
                        var response = apiService.loginUser(this.user);
                        response.then(function(data) {
                            if(data.result.answers){
                                SQLService.setPreferencias(data.result.answers);
                            }
                            $localstorage.set('username', data.result.email);
                            $localstorage.set('id', data.result.id);
                            $scope.closeModal('login');
                            $state.transitionTo('privacy');
                            $scope.getMatches();
                        }, function(data) {
                            if(data.error === true){
                                $ionicPopup.alert({
                                    title: 'Hot Franchise',
                                    template: data.message
                                });
                            }else{
                                $ionicPopup.alert({
                                    title: 'Hot Franchise',
                                    template: 'Something went wrong... try again later'
                                }); 
                            }
                        });
                    }else{
                        $ionicPopup.alert({
                            title: 'Hot Franchise',
                            template: "You are unable to access the Internet"
                        });  
                    }
                }else{
                    $ionicPopup.alert({
                        title: 'Hot Franchise',
                        template: 'Invalid email address'
                    });
                }
            }    
        }else{
            $ionicPopup.alert({
                title: 'Hot Franchise',
                template: 'Invalid email address'
            });
        }
        
    };

    $scope.getMatches = function(){
        var response = apiService.getMatches();
        response.then(function(data) {
            angular.forEach(data.result, function(value, key) {
              SQLService.setMatch(value.franq_id, value.result, value.franq_name);
            });
        });
    }
})

.controller('privacyCtrl', function ($scope, $state, $ionicLoading, $rootScope, $ionicPopup, SQLService, apiService, $window) {
    
    $scope.preferencias = [];

    $scope.loadPreferencias = function() {
        SQLService.allPreferencias().then(function (results) {
            $scope.preferencias = results;
        });
    }

    $scope.getFranquicias = function(){
        apiService.getFranquicias().then(function(data) {
            //SQLService.deleteFranquicias();
            for (var i=0; i< data.result.length; i++){
                SQLService.setFranquicia(data.result[i]);
            }
        }, function(message) {
            $ionicPopup.alert({
                title: 'Error',
                template: message
            });
        });
    };

    $scope.agreeTerms = function(){
        this.loadPreferencias();
        $scope.getFranquicias();
        $ionicLoading.show({
            template: 'Loading ...'
        });

        window.setTimeout(function() {
            if(Object.size($scope.preferencias) == 0)
            {
                $state.transitionTo('interview');
                 
            }else{
                $state.transitionTo('app.home');
            }       
            $ionicLoading.hide();
        
        },3200);

        
    };
})

.controller('publiCtrl', function ($scope, $state, $ionicLoading, $rootScope, $ionicPopup, $window, SQLService, apiService) {
    

    
})

.controller('interviewCtrl', function ($scope, $state, $ionicLoading, $rootScope, $ionicPopup, $window, SQLService, $http, apiService, $cordovaNetwork, $cordovaDialogs) {
    
    $scope.preferencias = {};
    $scope.showBackButton = false;

    $scope.backFranchise = function() {
        $state.transitionTo('app.home');
    }

    $scope.loadPreferencias = function() {
        SQLService.allPreferencias().then(function (results) {
            $scope.preferencias = results;
        });
    }

    $scope.loadQuestions = function() {
        apiService.getQuestions().then(function(questions) {
            $scope.questions = questions;
        });
    }

    $scope.$on('$ionicView.beforeEnter', function(){
        $ionicLoading.show({
            template: 'Loading ...'
        });
        $scope.loadQuestions();
        $scope.loadPreferencias();
        window.setTimeout(function() {
            if(Object.size($scope.preferencias) == 1){
                $scope.showBackButton = true;
                $scope.Answers = {};
                $scope.Answers[1] = $scope.preferencias[0].resp1; 
                $scope.Answers[2] = capitalizeFirstLetter($scope.preferencias[0].resp2);
                $scope.Answers[3] = capitalizeFirstLetter($scope.preferencias[0].resp3);
                $scope.Answers[4] = $scope.preferencias[0].resp4;
                $scope.Answers[5] = capitalizeFirstLetter($scope.preferencias[0].resp5);
                $scope.Answers[6] = $scope.preferencias[0].resp6;
                $scope.Answers[7] = $scope.preferencias[0].resp7;
                $scope.Answers[8] = $scope.preferencias[0].resp8;
                $scope.Answers[9] = $scope.preferencias[0].resp9;
                $scope.Answers[10] = $scope.preferencias[0].resp10;
            }else{
                $scope.showBackButton = false;
                $scope.Answers = {};
                $scope.Answers[1] = 5; 
                $scope.Answers[4] = 5;
                $scope.Answers[6] = 5;
                $scope.Answers[7] = 5;
                $scope.Answers[8] = 5;
                $scope.Answers[9] = 5;
                $scope.Answers[10] = 5;
            }

            $ionicLoading.hide();
        },2000);
    });

    //Verifico las respuestas de la encuesta.
    $scope.checkAnswer = function(){
        if(Object.size($scope.Answers) < 10){
            $ionicPopup.alert({
                title: 'Error',
                template: 'Some options are empty'
            });
        }else{
            $scope.Answers[1] = $scope.Answers[1].toString();
            $scope.Answers[2] = $scope.Answers[2].toLowerCase();
            $scope.Answers[3] = $scope.Answers[3].toLowerCase();
            $scope.Answers[4] = $scope.Answers[4].toString();
            $scope.Answers[5] = $scope.Answers[5].toLowerCase();
            $scope.Answers[6] = $scope.Answers[6].toString();
            $scope.Answers[7] = $scope.Answers[7].toString();
            $scope.Answers[8] = $scope.Answers[8].toString();
            $scope.Answers[9] = $scope.Answers[9].toString();
            $scope.Answers[10] = $scope.Answers[10].toString();
            SQLService.setPreferencias($scope.Answers);

            if($cordovaNetwork.isOnline()){
                var response = apiService.updateAnswers(this.Answers)
                response.then(function(data) {
                    $ionicLoading.hide();
                    /*
                    $ionicPopup.alert({
                        title: 'Hot Franchise',
                        template: 'Your preferences were saved successfully'
                    });

                    US/Canada 
                    - International
                    */

                    $cordovaDialogs.confirm("Your preferences were saved successfully \n"+
                                    "NEXT-Select Franchises.", 'Hot Franchise', ['US/Canada','International'])
                    .then(function(buttonIndex) {
                        // 'US/Canada' = 1, 'International' = 2
                        if(buttonIndex == 2){
                            $state.transitionTo('app.usfranchises');
                        }else
                            $state.transitionTo('app.international');  
                    });  

                    
                    
                }, function(data) {
                    $ionicLoading.hide();
                    if(data.error === true){
                        $ionicPopup.alert({
                            title: 'Network Error',
                            template: data.message
                        });
                    }else{
                        $ionicPopup.alert({
                            title: 'Network Error',
                            template: 'Something went wrong... try again later'
                        }); 
                    }
                });    
            }else{
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Hot Franchise',
                    template: 'Your preferences was saved successfully'
                });
                $state.transitionTo('app.home');
            }
        }
    }
})

.controller('homeCtrl', function ($scope, $state, $ionicLoading, $rootScope, $window, SQLService, $localstorage, $ionicViewSwitcher, apiService, $ionicPopup, $ionicHistory, $cordovaNetwork, $cordovaSplashscreen, $cordovaStatusbar, $cordovaInAppBrowser) {

    $scope.showMath = false;
    //$cordovaStatusbar.hide();

    $scope.toggleLeft = function(){
        $ionicSideMenuDelegate.toggleLeft();
    }

    $scope.backHome = function() {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.transitionTo('app.home');
    }

    $scope.loadFranquiciasUS = function() {
        SQLService.allFranquiciasUS().then(function (results) {
            $scope.franquiciasUS = results;
        });
    }

    $scope.loadFranquiciasInternational = function() {
        SQLService.allFranquiciasInternational().then(function (results) {
            $scope.franquiciasInter = results;
        });
    }

    $scope.loadPreferencias = function() {
        SQLService.allPreferencias().then(function (results) {
            $scope.preferencias = results;
        });
    }

    $scope.loadMatchs = function() {
        SQLService.allMatchs().then(function (results) {
            $scope.matchs = results;
        });
    }

    $scope.loadQuestions = function() {
        apiService.getQuestions().then(function(questions) {
            $scope.questions = questions;
        });
    }

    $scope.loadCountries = function() {
        SQLService.getAllCountries().then(function (results) {
            $scope.countries = results;
            var empty = {country: "All"}
            $scope.countries.unshift(empty);
            console.log($scope.countries);
        });
    }

    $scope.$on('$ionicView.beforeEnter', function(scopes, states){
        $scope.showMath = false;
        $scope.loadPreferencias();
        if(states.stateName === 'app.home'){
            if($scope.franquiciasUS != undefined){
                
            }
            else{
                $ionicLoading.show({
                    template: 'Loading ...'
                }); 
                $scope.loadPreferencias();
                $scope.loadFranquiciasUS();
                $scope.loadFranquiciasInternational();
                $scope.loadCountries();
                $scope.loadMatchs();
                window.setTimeout(function() {
                    $cordovaSplashscreen.hide();
                    $ionicLoading.hide();
                },2000); 
            }
        }else{
            if(states.stateName === 'app.match'){
                $scope.showMath = false;
            }
            if(states.stateName === 'app.franchise'){
                $scope.contact = {
                    name : "", phone: "", email: ""
                };
            }
            if(states.stateName === 'app.account'){
                $scope.password = {
                    current: "", new: "", new2: "", username: $localstorage.get('username')
                };
            }
            if(states.stateName === 'app.international'){
                
                $scope.interFranchise = {};

                $scope.interFranchise = {
                    pais: "All", search: ""
                };

                SQLService.allFranquiciasInternational().then(function (results) {
                    $scope.franquiciasInter = results;
                });
                
            }
            if(states.stateName === 'app.usfranchises'){
                
                $scope.usFranchise = { search: "" };

                $scope.loadFranquiciasUS();

            }
            if(states.stateName === 'app.news'){
                $ionicLoading.show({
                    template: 'Loading ...'
                });
                
                var response = apiService.getNews();
                response.then(function(data) {
                    $scope.news = data.result;
                    $ionicLoading.hide();
                }, function(data) {
                    $ionicLoading.hide();
                    //console.log(data);
                    if(data.error === true){
                        $ionicPopup.alert({
                            title: 'Error',
                            template: data.message
                        });
                    }else{
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Something went wrong... try again later'
                        }); 
                    }
                }); 
                
            }
        }
    });

    $scope.$on('$ionicView.loaded', function(scopes, states){
        
        if(states.stateName === 'app.match'){
            $ionicLoading.show({
                template: 'Loading ...'
            });

            window.setTimeout(function() {
                $ionicLoading.hide();
                $scope.showMath = true;
            },1500); 
        }
    });

    $scope.calc = function(franquicia) {

        if(Object.size($scope.preferencias) > 0){
            
            var totalPts = 0;
            var text = unescape(franquicia.answer)
            var obj = JSON.parse(text);
            totalPts += 10 - Math.abs( Number(obj[1]) - Number($scope.preferencias[0].resp1));
            totalPts += 10 - Math.abs( Number(obj[4]) - Number($scope.preferencias[0].resp4));
            totalPts += 10 - Math.abs( Number(obj[6]) - Number($scope.preferencias[0].resp6));
            totalPts += 10 - Math.abs( Number(obj[7]) - Number($scope.preferencias[0].resp7));
            totalPts += 10 - Math.abs( Number(obj[8]) - Number($scope.preferencias[0].resp8));
            totalPts += 10 - Math.abs( Number(obj[9]) - Number($scope.preferencias[0].resp9));
            totalPts += 10 - Math.abs( Number(obj[10]) - Number($scope.preferencias[0].resp10));
            if($scope.preferencias[0].resp2 === obj[2]){
                totalPts += 10;
            }else
                totalPts += 7;

            if($scope.preferencias[0].resp3 === obj[3]){
                totalPts += 10;
            }else
                totalPts += 5;
            if($scope.preferencias[0].resp5 === obj[5]){
                totalPts += 10;
            }else
                totalPts += 5;

            SQLService.setMatch(franquicia.id, totalPts, franquicia.name);
            this.loadMatchs();
            if($cordovaNetwork.isOnline()){
                var response = apiService.uploadMatch(franquicia.id, totalPts);
                response.then(function(data) {
                    $ionicLoading.hide();
                    
                    SQLService.getMatch(franquicia.id).then(function (results) {
                        $scope.createTemporalMatch(results[0]);
                    });

                    $state.go('app.match', {'idMatch': franquicia.id});
                }, function(data) {
                    $ionicLoading.hide();
                    //console.log(data);
                    if(data.error === true){
                        $ionicPopup.alert({
                            title: 'Error',
                            template: data.message
                        });
                    }else{
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Something went wrong... try again later'
                        }); 
                    }
                });    
            }else{
                $ionicLoading.hide();

                SQLService.getMatch(franquicia.id).then(function (results) {
                    $scope.createTemporalMatch(results[0]);
                });

                $state.go('app.match', {'idMatch': franquicia.id});
            }
        }else{
            $ionicPopup.alert({
                title: 'Error',
                template: "You haven’t taken the compatibility test."
            });
        }
    }
            
    $scope.openVideo = function(link){
        //window.open(link, '_blank', 'location=no');
        
        if ( ionic.Platform.isAndroid() ){
            var options = {
              location: 'false',
              clearcache: 'yes',
              toolbar: 'yes',
              closebuttoncaption: 'Contact Franchisor'
            };

            $cordovaInAppBrowser.open(link, '_blank', options)
              .then(function(event) {
                console.log("cargo")
              })
              .catch(function(event) {
                console.log("ERROR")
              });

            $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event){
                console.log("Le dio done");
                $state.go('app.contact'); 
            });

        }else{
            var ref = window.open(link, '_blank','location=no');
            ref.addEventListener('exit', function(event) { 
                $state.go('app.contact');
            } ); 
        }
    }

    $scope.openURL = function(link){
        //console.log(link);
        window.open(link, '_blank', 'location=no'); 
    }

    $scope.sendEmail = function(infoContact, franchise){
        if($scope.contact.email){
            if($scope.contact.name.length === 0 || $scope.contact.email.length === 0 || $scope.contact.phone.length === 0){
                $ionicPopup.alert({
                    title: 'Hot Franchise',
                    template: 'Some fields are empty'
                });
            }else{
                if(isValidEmail($scope.contact.email)){
                    console.log("Puede enviar email");
                    if($cordovaNetwork.isOnline()){
                        var response = apiService.sendEmail(infoContact, franchise);
                        response.then(function(data) {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Hot Franchise',
                                template: "Thank you for submitting your franchise inquiry. Someone from our office will soon be contacting you to provide additional information."
                            });
                        }, function(data) {
                            $ionicLoading.hide();
                            //console.log(data);
                            if(data.error === true){
                                $ionicPopup.alert({
                                    title: 'Error',
                                    template: data.message
                                });
                            }else{
                                $ionicPopup.alert({
                                    title: 'Error',
                                    template: 'Something went wrong... try again later'
                                }); 
                            }
                        });    
                    }else{
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Hot Franchise',
                            template: "You are unable to access the Internet"
                        }); 
                    }
                }else{
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Invalid email address'
                    });
                }
            }
        }else{
            $ionicPopup.alert({
                title: 'Error',
                template: 'Invalid email address'
            });
        }
    }

    $scope.changePassword = function(password){
        if(password){
            if($scope.password.current.length === 0 || $scope.password.new.length === 0 || $scope.password.new2.length === 0){
                $ionicPopup.alert({
                    title: 'Hot Franchise',
                    template: 'Some fields are empty'
                });
            }else{
                if($scope.password.new === $scope.password.new2){
                    console.log("Puede cambiar el password");
                    if($cordovaNetwork.isOnline()){
                        var response = apiService.changePassword($scope.password);
                        response.then(function(data) {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Hot Franchise',
                                template: "Your password was changed successfully."
                            });
                            $scope.password = {
                                current: "", new: "", new2: "", username: $localstorage.get('username')
                            };
                        }, function(data) {
                            $ionicLoading.hide();
                            //console.log(data);
                            if(data.error === true){
                                $ionicPopup.alert({
                                    title: 'Error',
                                    template: data.message
                                });
                            }else{
                                $ionicPopup.alert({
                                    title: 'Error',
                                    template: 'Something went wrong... try again later'
                                }); 
                            }
                        });    
                    }else{
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Hot Franchise',
                            template: "You are unable to access the Internet"
                        }); 
                    }
                }else{
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'the passwords you entered do not match'
                    });
                }
            }
        }
    }

    $scope.goInterview = function(){
        $ionicViewSwitcher.nextDirection('forward'); // 'forward', 'back', etc.
        $state.transitionTo('interview');
    }

    $scope.createTemporalMatch = function(match){
        $scope.temporalMatch = match;
    }

    $scope.goBackMatch = function(match){
        $scope.createTemporalMatch(match);
        $ionicLoading.show({
            template: 'Loading...'
        });

        $ionicHistory.goBack();
        window.setTimeout(function() {
            $scope.showMath = true;
            $ionicLoading.hide();
        },2000);
        
    }

    $scope.goBackHome = function(typeFranchise){
        
        $ionicLoading.show({
            template: 'Loading...'
        });
        
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        console.log(typeFranchise)
        if(typeFranchise == 2)
            $state.go('app.usfranchises'); 
        else
            $state.go('app.international'); 
        
        window.setTimeout(function() {
            $ionicLoading.hide();
        },1500);
    }

    $scope.showDifferences = function(match) {

        $ionicLoading.show({
            template: 'Loading...'
        });

        $scope.showMath = false;
        $scope.franquicia = {};
        $scope.answerFranchise = {};
        $scope.match = {};

        $scope.loadQuestions();
        SQLService.getFranquicia(match.id).then(function (results) {
                $scope.franquicia = results[0];
        });

        $scope.match = match;
        this.createTemporalMatch(match);
        window.setTimeout(function() {
            var text = unescape($scope.franquicia.answer)
            $scope.answerFranchise = JSON.parse(text);
            $state.transitionTo('app.differences');
            $ionicLoading.hide();
        },1000);
    }

    $scope.showContactFranchistor = function(){
        
        $state.go('app.contact'); 
    }    

    $scope.showContact = function(idFranquicia, franquicia){
        $ionicLoading.show({
            template: 'Loading...'
        });

        $scope.franquicia = {};

        if(franquicia === null){
            SQLService.getFranquicia(idFranquicia).then(function (results) {
                $scope.franquicia = results[0];
            });

            window.setTimeout(function() {
                $state.go('app.franchise', {'idFranchise': idFranquicia}); 
                $ionicLoading.hide();   
            },1000);
            
        }else{
            $scope.franquicia = franquicia;
            window.setTimeout(function() {
                $state.go('app.franchise', {'idFranchise': franquicia.id}); 
                $ionicLoading.hide();   
            },1000);
        }
    }

    $scope.getFranchisesByName = function(franchise, typeFranchise) {
        if(typeFranchise == 'us'){
            SQLService.findUsFranchisesByName(franchise.search).then(function (results) {
                //console.log(results)
                $scope.franquiciasUS = results;
            });
        }else{
            if(franchise.search){
                if(franchise.search.length > 0){
                    if(franchise.pais === "All"){
                        console.log("findInternationalFranchisesByName "+franchise.search);
                        SQLService.findInternationalFranchisesByName(franchise.search).then(function (results) {
                            $scope.franquiciasInter = results;
                            console.log(results);
                        });    
                    }else{
                        console.log("findInternationalFranchisesByNameAndCountry "+franchise.search+ " "+franchise.pais);
                        SQLService.findInternationalFranchisesByNameAndCountry(franchise.search, franchise.pais).then(function (results) {
                            $scope.franquiciasInter = results;
                            console.log(results);
                        });  
                    }
                    
                }else{
                    if(franchise.pais != "All"){
                        console.log("findInternationalFranchisesByCountry "+franchise.pais);
                        SQLService.findInternationalFranchisesByCountry(franchise.pais).then(function (results) {
                            console.log(results);
                            $scope.franquiciasInter = results;
                        });  
                    }
                }
            }else{
                if(franchise.pais != "All"){
                        console.log("findInternationalFranchisesByCountry "+franchise.pais);
                        SQLService.findInternationalFranchisesByCountry(franchise.pais).then(function (results) {
                            $scope.franquiciasInter = results;
                        });
                }else{
                    SQLService.allFranquiciasInternational().then(function (results) {
                        $scope.franquiciasInter = results;
                    });
                    
                }
            }
        }
    };
})

.controller('logoutCtrl', function ($scope, $state, $ionicLoading, $rootScope, $window, SQLService, $localstorage, $ionicViewSwitcher, apiService, $ionicPopup) {

    $scope.logout = function(){
        $ionicLoading.show({
            template: 'Logging out...'
        });

        window.setTimeout(function() {
            SQLService.logout();
            SQLService.deleteMatchs();
            $localstorage.set('username', '');
            $localstorage.set('id', '');
            $ionicViewSwitcher.nextDirection('back'); // 'forward', 'back', etc.
            $state.go('index');
            $ionicLoading.hide();
        },1000);
    }
})

Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

function isValidEmail(mail)
{
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail);
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

